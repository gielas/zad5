package services;

import domain.*;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserService {
	
	public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
	    List<User> usersWhoHaveMoreThanOneAddress = users.stream()
				        .filter(user -> user.getPersonDetails().getAddresses().size() > 1)
				        .collect(Collectors.toList());
	    return usersWhoHaveMoreThanOneAddress;  
    }

	public static Person findOldestPerson(List<User> users) {
	    Person oldestPerson = users.stream()
                        .map(user -> user.getPersonDetails())
                        .max(Comparator.comparing(Person::getAge))
                        .get();
        return oldestPerson;
    }

    public static User findUserWithLongestUsername(List<User> users) {
        User longestUsername = users.stream()
    				    .max((u1, u2) -> u1.getName().length() - u2.getName().length()) 
    				    .get();
	    return longestUsername;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        String usersAbove18 = users.stream() 
	    				.filter(user -> user.getPersonDetails().getAge() > 18)
	                    .map(user -> user.getPersonDetails().getName() + " " + user.getPersonDetails().getSurname())
	    				.collect(Collectors.joining(", "));			   
		return usersAbove18; 
    }

   public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
	    List<String> sortedPermissions = users.stream() 
	    			    .filter(user -> user.getPersonDetails().getName().startsWith("A"))
	    			    .map(user -> user.getPersonDetails().getRole().getPermissions())
    				    .flatMap(Collection::stream)
    				    .sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
    				    .map(permission -> permission.getName())    						
    				    .collect(Collectors.toList());
		return sortedPermissions;   
    }

      public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
	        users.stream()
	                    .filter(user -> user.getPersonDetails().getSurname().startsWith("S"))
	                    .map(user -> user.getPersonDetails().getRole().getPermissions())
	                    .flatMap(Collection::stream)
	                    .map(permission -> permission.getName().toUpperCase())
	        			.forEach(System.out::println);
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        Map<Role, List<User>> groupByRole = users.stream()
                        .collect(Collectors.groupingBy((user -> user.getPersonDetails().getRole())));
        return groupByRole;  
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
		Map<Boolean, List<User>> underAndOver18 = users.stream()
                        .collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() >= 18)); 
        return underAndOver18;
    }
}