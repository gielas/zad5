import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import domain.*;
import services.UserService;

public class Tests {
	
	private static List<User> users;
	
	private static void initUsersList() {
		
	Permission write = new Permission().setName("zapis");
    Permission read = new Permission().setName("odczyt");
    Permission execute = new Permission().setName("wykonywanie");

    List<Permission> superAdminPermissions = Arrays.asList(write, read, execute);
    List<Permission> adminPermissions = Arrays.asList(write, read);
    List<Permission> userPermissions = Arrays.asList(read);
        
	Role superAdmin = new Role().setPermissions(superAdminPermissions);
	Role admin = new Role().setPermissions(adminPermissions);
	Role user = new Role().setPermissions(userPermissions);
  
    Address adres1 = new Address().setCity("Gdansk");
	Address adres2 = new Address().setCity("Gdynia");
	Address adres3 = new Address().setCity("Wejherowo");

	List<Address> listaAdresow1 = new ArrayList<Address>();
	List<Address> listaAdresow2 = new ArrayList<Address>();
	List<Address> listaAdresow3 = new ArrayList<Address>();

	listaAdresow1.add(adres1);
	listaAdresow2.add(adres1);
	listaAdresow2.add(adres2);
	listaAdresow3.add(adres1);
	listaAdresow3.add(adres2);
	listaAdresow3.add(adres3);
		
	Person person1 = new Person("Damian", "Gielazyn", 23, superAdmin).setAddresses(listaAdresow1);
	Person person2 = new Person("Olga", "Bednarek", 17, admin).setAddresses(listaAdresow1);
	Person person3 = new Person("Amelia", "Kin", 18, admin).setAddresses(listaAdresow3);
	Person person4 = new Person("Adam", "Szynkowski", 30, user).setAddresses(listaAdresow2);
	Person person5 = new Person("Daniel", "Grzesiak", 15, user).setAddresses(listaAdresow1);
		
	User user1 = new User().setName("damGiel").setPersonDetails(person1);
	User user2 = new User().setName("olgBedn").setPersonDetails(person2);
	User user3 = new User().setName("amelKin").setPersonDetails(person3);
	User user4 = new User().setName("adaSzyn").setPersonDetails(person4);
	User user5 = new User().setName("danGrzes").setPersonDetails(person5);
		
		users = Arrays.asList(user1, user2, user3, user4, user5);
	}

	@Test
	public void FindUsersWhoHaveMoreThanOneAddress() {
		initUsersList();
		List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		assertTrue(result.size() == 2);
		assertSame(result.size(), 2);
		System.out.println("FindUsersWhoHaveMoreThanOneAddress = " + result);		
	}
	
	@Test
	public void findOldestPerson() {
		initUsersList();
		Person result = UserService.findOldestPerson(users);
		assertTrue(result.getAge() == 30);
		assertSame(result.getName(), "Adam");
		System.out.println("findOldestPerson = " + result);
	}
	
	@Test
	public void findUserWithLongestUsername() {
		initUsersList();
		User result = UserService.findUserWithLongestUsername(users);
		assertTrue(result.getName().length() == 8);
		assertSame(result.getName(), "danGrzes");
		System.out.println("findUserWithLongestUsername = " + result.getName());
	}
	
	@Test
	public void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		initUsersList();
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		assertTrue(result.equals("Damian Gielazyn, Adam Szynkowski"));
		System.out.println("getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18 = " + result);
	}
	
	@Test
	public void getSortedPermissionsOfUsersWithNameStartingWithA() {
		initUsersList();
		List<String> result = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		assertTrue(result.size() == 3);
		assertSame(result.size(), 3);
		System.out.println("getSortedPermissionsOfUsersWithNameStartingWithA = " + result);
	}
	
	@Test
	public void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS() {
		initUsersList();
		System.out.println("printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS = ");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
	}
	
	@Test
	public void groupUsersByRole() {
		initUsersList();
		Map<Role, List<User>> result = UserService.groupUsersByRole(users);
		System.out.println("groupUsersByRole = " + result);
	}
	
	@Test
	public void partitionUserByUnderAndOver18() {
		initUsersList();
		Map<Boolean, List<User>> result = UserService.partitionUserByUnderAndOver18(users);
		System.out.println("partitionUserByUnderAndOver18 = " + result);
	}
}
	